# Morello RootFS Images based on Debian GNU/Linux
This project provides a set of rootfs images based on Debian GNU/Linux that can be used on Morello SoCs or Morello FVP.  

The image contains:
- A bootable GPT partition
- The latest Morello kernel
- The [Morello development environment](https://git.morello-project.org/morello/morello-sdk)

## Latest Images
* [Latest Image for Morello SoC](https://git.morello-project.org/morello/morello-rootfs-images/-/jobs/artifacts/morello/mainline/raw/morello-soc.tar.xz?job=build-morello-rootfs-images)
* [Latest Image for Morello FVP](https://git.morello-project.org/morello/morello-rootfs-images/-/jobs/artifacts/morello/mainline/raw/morello-fvp.tar.xz?job=build-morello-rootfs-images)
* [Image Registry](https://git.morello-project.org/morello/morello-rootfs-images/-/packages)

### Decompress an Image
To decompress and verify a downloaded image (FVP or SoC) follow the steps below:
```
$ tar -xJf ~/<PATH>/morello-soc.tar.xz
$ ls morello-soc/*
morello-soc/morello-soc.img  morello-soc/version.txt
$ cat morello-soc/version.txt
version: <date>
```
